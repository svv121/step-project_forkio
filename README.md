## Name of the project:

# ***Forkio***

## Participants of the project:

1. Volodymyr Shestopalov

## The list of technologies being used:

1. HTML
2. SCSS
3. CSS
4. JS
5. Gulp
6. Grid
7. Git
8. BEM

## Tasks of the project

* Gulp assembly
* Header, Menu, Burger Menu, Download button
* Block 'Revolutionary Editor'
* Section 'Here is what you get'
* Section 'People Are Talking About Fork'
* Section 'Fork Subscription Pricing'

## GitLab Pages address

https://svv121.gitlab.io/step-project_forkio/dist/